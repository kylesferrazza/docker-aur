{
  description = "docker-aur";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; };
      in {
        defaultPackage = (pkgs.writeShellScriptBin "docker-aur" ''
          docker pull registry.gitlab.com/kylesferrazza/docker-aur:latest
          ${pkgs.xorg.xhost}/bin/xhost +local:root
          docker run \
            --env="DISPLAY" \
            --env="QT_X11_NO_MITSHM=1" \
            --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
            -v $PWD:/app \
            --rm \
            -it registry.gitlab.com/kylesferrazza/docker-aur:latest
          ${pkgs.xorg.xhost}/bin/xhost -local:root
        '');
      });
}
