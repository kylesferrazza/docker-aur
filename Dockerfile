# syntax=docker/dockerfile:latest

FROM archlinux:latest
RUN <<EOF
  echo en_US.UTF-8 UTF-8 | tee -a /etc/locale.gen
  echo LANG=en_US.UTF-8 | tee /etc/locale.conf
  locale-gen
  mkdir -pv /var/lib/pacman
  echo -e 'Server = http://mirrors.acm.wpi.edu/archlinux/$repo/os/$arch\nServer = http://mirrors.advancedhosters.com/archlinux/$repo/os/$arch\nServer = http://mirrors.aggregate.org/archlinux/$repo/os/$arch\nServer = https://america.mirror.pkgbuild.com/$repo/os/$arch' > /etc/pacman.d/mirrorlist
  pacman --noconfirm --needed -Syu base base-devel pacman-contrib git namcap
  sed -i /etc/makepkg.conf -e 's/\.pkg\.tar\.xz/.pkg.tar/'
  echo "%wheel ALL = (ALL) NOPASSWD: ALL" > /etc/sudoers
  groupmod -g 100 wheel
  useradd -m -g wheel kyle
  echo "alias updsrcinfo='makepkg --printsrcinfo > .SRCINFO'" >> /home/kyle/.bashrc
EOF

USER kyle
VOLUME /app
WORKDIR /app
